<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

use App\Tickets;

use Auth;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Auth check making sure user is logged in
        if(Auth::check() == false){
            return redirect('./login');
        }

        $uid = Auth::user()->id;
        $tickets = DB::table('tickets')->select('tickets.id','tickets.title','tickets.project','tickets.level','tickets.status','tickets.created_at')->where('tickets.cid', '=', $uid)->orderBy('created_at', 'desc')->get();
        $total = DB::table('tickets')->select('id')->where('cid', '=', $uid)->count();
        $open = DB::table('tickets')->select('id')->where('cid', '=', $uid)->where('status', '=', '0')->count();
        $pending = DB::table('tickets')->select('id')->where('cid', '=', $uid)->where('status', '=', '1')->count();
        $closed = DB::table('tickets')->select('id')->where('cid', '=', $uid)->where('status', '=', '2')->count();
        return view('tickets.index', compact('tickets', 'total', 'open', 'pending', 'closed'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Auth check making sure user is logged in
        if(Auth::check() == false){
            return redirect('./login');
        }

        $uid = Auth::user()->id;
        $services = DB::table('services')->select('services.id','services.name')->where('services.cid', '=', $uid)->orderBy('created_at', 'desc')->get();

        return view('tickets.new', compact('services'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //var_dump($_POST);
        $cid = Auth::user()->id;
        
        $title = $request->get('title');
        $priority = $request->get('priority');
        $description = $request->get('description');
        $service = $request->get('service');
        if($description == "<br>" || $description == "" || $description == " "){ $description = $title . "<br>"; }
        
        $files = $request->file('files');
        $atch = "";

            if($request->hasFile('files'))
            {
                
                $um = Auth::user()->email;
                $time = \Carbon\Carbon::now()->timestamp;
                $fln = "" . $time . $um;
                $hash = hash('ripemd160', $fln);
                $destination = "attachments/" . $hash;
                $atch = $hash;


                foreach ($files as $file) {
                    //var_dump($file);
                    $filename = $file->getClientOriginalName();
                    
                    $file->move($destination, $filename);
                }
            }

            //NEEDS THE PROJECT INSERT AND LANGUAGE INSERT
            $tid = DB::table('tickets')->insertGetId(
                ['cid' => $cid, 'uid' => '', 'project' => '', 'language' => '', 'service' => $service, 'title' => $title, 'level' => $priority, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]
            );
            DB::table('comments')->insert(
                ['tid' => $tid, 'poster' => $cid, 'comment' => $description, 'attachments' => $atch, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]
            );

            return redirect('tickets');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Auth check making sure user is logged in
        if(Auth::check() == false){
            return redirect('./login');
        }
        if($id == "view"){
            return redirect('tickets');
        }
        $tid = $id;
        $ticket = DB::table('tickets')->select('tickets.id','tickets.title','tickets.level','tickets.status','tickets.created_at','services.name','users.name as asign','tickets.service')->leftJoin('services','services.id','=','tickets.service')->leftJoin('users','users.id','=','tickets.uid')->where('tickets.id', '=', $id)->get();
        $comments = DB::table('comments')->select('comments.id', 'comments.comment', 'comments.attachments', 'comments.created_at', 'users.name', 'users.email', 'users.level', 'users.avatar')->leftJoin('users','users.id','=','comments.poster')->where('comments.tid', '=', $id)->get();
        

		return view('tickets.show', compact('tid', 'ticket', 'comments'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}