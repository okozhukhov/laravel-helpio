<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

use Auth;

class InboxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Auth check making sure user is logged in
        if(Auth::check() == false){
            return redirect('./login');
        }

        $uid = Auth::user()->id;
        $emails = DB::table('email_services')->select('email_services.id','email_services.email')->where('email_services.cid', '=', $uid)->orderBy('created_at', 'desc')->get();

        return view('inbox.index', compact('emails'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Auth check making sure user is logged in
        if(Auth::check() == false){
            return redirect('./login');
        }   

        $em = base64_decode(strrev($id));
        $uid = Auth::user()->id;
        $apidata = DB::table('email_services')->select('email_services.eid','email_services.apikey','email_services.region')->where([['email_services.cid', '=', $uid], ['email_services.email', '=', $em]])->orderBy('created_at', 'desc')->get();
        $apidata = $apidata->toArray();
        
        if (empty($apidata)) {
            return redirect('./inbox');
        }
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://mail.zoho.". $apidata[0]->region ."/api/accounts/". $apidata[0]->eid ."/messages/view?&limit=10&AUTHTOKEN=". $apidata[0]->apikey);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

        $headers = array();	
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);

        curl_setopt($ch, CURLOPT_URL, "https://mail.zoho.eu/api/accounts/". $apidata[0]->eid ."/messages/view?status=unread&limit=100&AUTHTOKEN=". $apidata[0]->apikey);
        
        $tocount = curl_exec($ch);

        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }

        curl_close ($ch);

        $json = json_decode($result);
        $jsoncount = json_decode($tocount);
        //$json = $result['data'];


        $data = $json->data;
        array_pop($data);
        $unread = sizeof($jsoncount->data) - 1;
        
        return view('inbox.emails', compact('data', 'em', 'unread'));
        //print_r($data->data['0']->subject);

        //echo "<pre>" . $result . "</pre>";
        //Y29udGFjdEBvdXRlcmZsb3cuY29t
        
    }

    public function view($email, $id){
        //Auth check making sure user is logged in
        if(Auth::check() == false){
            return redirect('./login');
        }   

        $em = base64_decode(strrev($id));

        $unread = 0;
        return view('inbox.view', compact('em', 'unread'));
        
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
