<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>EVONUƎ @yield('subtitle')</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
	<meta content="Coderthemes" name="author" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />

	<!-- App favicon -->
	<link rel="shortcut icon" href="{{ URL::asset('favicon.png') }}">

	<!-- App css -->
	<link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ URL::asset('css/icons.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ URL::asset('css/style.css') }}" rel="stylesheet" type="text/css" />

	<script src="{{ URL::asset('js/modernizr.min.js') }}"></script>

  @yield('hextra')

</head>

<body>

	<!-- Navigation Bar-->
	<header id="topnav">
		<div class="topbar-main">
			<div class="container-fluid">

				<!-- Logo container-->
				<div class="logo">
					<a href="/" class="logo">
						<img src="{{ URL::asset('images/evonue_logo.png') }}" alt="" height="22">
					</a>

				</div>
				<!-- End Logo container-->


				<div class="menu-extras topbar-custom">

					<ul class="list-unstyled topbar-right-menu float-right mb-0">

						<li class="menu-item">
							<!-- Mobile menu toggle-->
							<a class="navbar-toggle nav-link">
								<div class="lines">
									<span></span>
									<span></span>
									<span></span>
								</div>
							</a>
							<!-- End mobile menu toggle-->
						</li>
						@yield('langsec')
						<li class="dropdown notification-list">
							<a class="nav-link dropdown-toggle waves-effect nav-user" data-toggle="dropdown" href="#" role="button"
							   aria-haspopup="false" aria-expanded="false">
								<img src="{{ URL::asset('images/users/avatar-1.jpg') }}" alt="user" class="rounded-circle"> <span class="ml-1 pro-user-name">Maxine K <i class="mdi mdi-chevron-down"></i> </span>
							</a>
							<div class="dropdown-menu dropdown-menu-right profile-dropdown ">
								<!-- item-->
								<div class="dropdown-item noti-title">
									<h6 class="text-overflow m-0">Welcome !</h6>
								</div>

								<!-- item-->
								<a href="javascript:void(0);" class="dropdown-item notify-item">
									<i class="fi-head"></i> <span>My Account</span>
								</a>

								<!-- item-->
								<a href="javascript:void(0);" class="dropdown-item notify-item">
									<i class="fi-cog"></i> <span>Settings</span>
								</a>

								<!-- item-->
								<a href="javascript:void(0);" class="dropdown-item notify-item">
									<i class="fi-help"></i> <span>Support</span>
								</a>

								<!-- item-->
								<a href="javascript:void(0);" class="dropdown-item notify-item">
									<i class="fi-lock"></i> <span>Lock Screen</span>
								</a>

								<!-- item-->
								<a href="javascript:void(0);" class="dropdown-item notify-item">
									<i class="fi-power"></i> <span>Logout</span>
								</a>

							</div>
						</li>
					</ul>
				</div>
				<!-- end menu-extras -->

				<div class="clearfix"></div>

			</div> <!-- end container -->
		</div>
		<!-- end topbar-main -->
		@yield('navigation')
	</header>
	<!-- End Navigation Bar-->


	<div class="wrapper">
		<div class="container-fluid">

			@yield('header')
      		@yield('content')
			

		</div> <!-- end container -->
	</div>
	<!-- end wrapper -->


	<!-- Footer -->
	<footer class="footer">
		<div class="container">
			<div class="row">
				<div class="col-12 text-center">
        2018 © Evonue. - evonue.com
				</div>
			</div>
		</div>
	</footer>
	<!-- End Footer -->

	<!-- jQuery  -->
	<script src="{{ URL::asset('js/jquery.min.js') }}"></script>
	<script src="{{ URL::asset('js/popper.min.js') }}"></script>
	<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
	<script src="{{ URL::asset('js/waves.js') }}"></script>
	<script src="{{ URL::asset('js/jquery.slimscroll.js') }}"></script>

	<!-- App js -->
	<script src="{{ URL::asset('js/jquery.core.js') }}"></script>
	<script src="{{ URL::asset('js/jquery.app.js') }}"></script>

  @yield('fextra')

</body>
</html>