@section('navigation')
<div class="navbar-custom">
			<div class="container-fluid">
				<div id="navigation">
					<ul class="navigation-menu">

						<li class="has-submenu">
							<a href="/"><i class="icon-speedometer"></i>{{ __("Dashboard") }}</a>
						</li>

						<li class="has-submenu">
							<a href="/tickets"><i class="icon-tag"></i>{{ __("Tickets") }}</a>
							<ul class="submenu">
								<li><a href="/tickets/create">{{ __("Open a new ticket") }}</a></li>
							</ul>
						</li>

						<li class="has-submenu">
							<a href="/inbox"><i class="icon-drawar"></i>{{ __("Inbox") }}</a>
						</li>

					</ul>
                    
				</div> 
			</div> 
		</div>
@endsection