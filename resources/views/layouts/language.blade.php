@section('langsec')
<li class="dropdown notification-list hide-phone">
<a class="nav-link dropdown-toggle waves-effect nav-user" data-toggle="dropdown" href="#" role="button"
    aria-haspopup="false" aria-expanded="false">
    <?php
        $ltxt = "";
        if(session()->get('applocale') == "en" || session()->get('applocale') == NULL) {
            $ltxt = "English";
        }
        if(session()->get('applocale') == "el") {
            $ltxt = "Ελληνικά";
        }
    ?>
    <i class="mdi mdi-earth"></i> <?php echo $ltxt; ?>  <i class="mdi mdi-chevron-down"></i>
</a>
<div class="dropdown-menu dropdown-menu-right">
    <?php if(session()->get('applocale') != "en" || session()->get('applocale') == NULL) { ?>
    <a href="{{ route('lang.switch', 'en') }}" class="dropdown-item">
        English
    </a>
    <?php
    }if(session()->get('applocale') != "el") {
    ?>
    <a href="{{ route('lang.switch', 'el') }}" class="dropdown-item">
        Ελληνικά
    </a>
    <?php } ?>
</div>
</li>
@endsection