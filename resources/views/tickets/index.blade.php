@extends('layouts.app')
@extends('layouts.language')
@extends('layouts.menu')
@section('subtitle')
 - Tickets
@endsection

@section('header')
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="btn-group pull-right">
                <ol class="breadcrumb hide-phone p-0 m-0">
                    <li class="breadcrumb-item"><a href="/">Evonue</a></li>
                    <li class="breadcrumb-item active">Tickets</li>
                </ol>
            </div>
            <h4 class="page-title">Tickets</h4>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row">
                    <div class="col-12">
                        <div class="card-box">
                            <h4 class="header-title">Manage Tickets</h4>

                            <div class="text-center mt-4 mb-4">
                                <div class="row">
                                    <div class="col-xs-6 col-sm-3">
                                        <div class="card-box widget-flat border-custom bg-custom text-white">
                                            <i class="fi-tag"></i>
                                            <h3 class="m-b-10"><?php echo $total; ?></h3>
                                            <p class="text-uppercase m-b-5 font-13 font-600">{{ __("Total tickets") }}</p>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-3">
                                        <div class="card-box bg-success widget-flat border-success text-white">
                                            <i class="fi-archive"></i>
                                            <h3 class="m-b-10"><?php echo $open; ?></h3>
                                            <p class="text-uppercase m-b-5 font-13 font-600">{{ __("Open tickets") }}</p>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-3">
                                        <div class="card-box widget-flat border-purple bg-purple text-white">
                                            <i class="fi-help"></i>
                                            <h3 class="m-b-10"><?php echo $pending; ?></h3>
                                            <p class="text-uppercase m-b-5 font-13 font-600">{{ __("Pending tickets") }}</p>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-3">
                                        <div class="card-box bg-secondary widget-flat border-secondary text-white">
                                            <i class="fi-delete"></i>
                                            <h3 class="m-b-10"><?php echo $closed; ?></h3>
                                            <p class="text-uppercase m-b-5 font-13 font-600">{{ __("Closed tickets") }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <table class="table table-hover m-0 tickets-list table-actions-bar dt-responsive nowrap" cellspacing="0" width="100%" id="datatable">
                                <thead>
                                <tr>
                                    <th>{{ __("No.") }}</th>
                                    <th>{{ __("Subject") }}</th>
                                    <th>{{ __("Priority") }}</th>
                                    <th>{{ __("Status") }}</th>
                                    <th>{{ __("Date") }}</th>
                                    <th class="hidden-sm">{{ __("Actions") }}</th>
                                </tr>
                                </thead>

                                <tbody>
                                <?php foreach($tickets as $ticket){ ?>
                                <tr>
                                    <td><b><?php echo $ticket->id; ?></b></td>
                                    

                                    <td>
                                        <a href="/tickets/view/<?php echo $ticket->id; ?>"><?php echo $ticket->title; ?></a>
                                    </td>

                                    <td>
                                        <?php $level = $ticket->level;
                                            if($level == '1'){
                                                echo '<span class="badge badge-custom">'. __("Low") .'</span>';
                                            }
                                            if($level == '2'){
                                                echo '<span class="badge badge-primary">'. __("Medium") .'</span>';
                                            }
                                            if($level == '3'){
                                                echo '<span class="badge badge-warning">'. __("High") .'</span>';
                                            }
                                            if($level == '4'){
                                                echo '<span class="badge badge-danger">'. __("Urgent") .'</span>';
                                            }

                                        ?>
                                    </td>

                                    <td>
                                        <?php

                                            $status = $ticket->status;
                                            if($status == '0'){
                                                echo '<span class="badge badge-success">'. __("Open") .'</span>';
                                            }
                                            if($status == '1'){
                                                echo '<span class="badge badge-purple">'. __("Pending") .'</span>';
                                            }
                                            if($status == '2'){
                                                echo '<span class="badge badge-secondary">'. __("Closed") .'</span>';
                                            }
                                            ?></
                                    </td>

                                    <td>
                                    <?php echo date('d.m.Y', strtotime($ticket->created_at));?>
                                    </td>

                                    <td>
                                        <div class="btn-group dropdown">
                                            <a href="javascript: void(0);" class="table-action-btn dropdown-toggle arrow-none btn btn-light btn-sm" data-toggle="dropdown" aria-expanded="false"><i class="mdi mdi-dots-horizontal"></i></a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="#"><i class="mdi mdi-pencil mr-2 text-muted font-18 vertical-middle"></i>Edit Ticket</a>
                                                <a class="dropdown-item" href="#"><i class="mdi mdi-check-all mr-2 text-muted font-18 vertical-middle"></i>Close</a>
                                                <a class="dropdown-item" href="#"><i class="mdi mdi-delete mr-2 text-muted font-18 vertical-middle"></i>Remove</a>
                                                <a class="dropdown-item" href="#"><i class="mdi mdi-star mr-2 font-18 text-muted vertical-middle"></i>Mark as Unread</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div><!-- end col -->
                </div>
        
@endsection

@section('hextra')
<link href="{{ URL::asset('plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ URL::asset('plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('fextra')
<script src="{{ URL::asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('plugins/datatables/dataTables.responsive.min.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $('#datatable').dataTable({ "order": [[ 0, "desc" ]] });
    });
</script>
@endsection