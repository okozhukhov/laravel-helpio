@extends('layouts.app')
@extends('layouts.language')
@extends('layouts.menu')
@section('subtitle')
 - Tickets
@endsection

@section('header')
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="btn-group pull-right">
                <ol class="breadcrumb hide-phone p-0 m-0">
                    <li class="breadcrumb-item"><a href="/">Evonue</a></li>
                    <li class="breadcrumb-item"><a href="/tickets">Tickets</a></li>
                    <li class="breadcrumb-item active">{{ __('Open New') }}</li>
                </ol>
            </div>
            <h4 class="page-title">Tickets</h4>
        </div>
    </div>
</div>
@endsection

@section('content')

<div class="row">
                    <div class="col-12">
                        <div class="card-box">
                            
                            <div class="row">
                                <div class="col-12">
                                    <div class="p-20">
                                    <form class="form-horizontal" role="form" method="post" action="{{url('tickets')}}" enctype="multipart/form-data">
                                                                        
                                            {{csrf_field()}}

                                            <div class="form-group row">
                                                <label class="col-1 col-form-label">{{ __('Issue') }}</label>
                                                <div class="col-7">
                                                    <input type="text" class="form-control" placeholder="{{ __('A short title for your issue') }}" name="title" required>
                                                </div>
                                                <label class="col-2 col-form-label">{{ __('Priority') }}</label>

                                                <div class="col-2">
                                                    <select class="selectpicker" data-style="btn-custom btn-block waves-effect" id="prio" name="priority">
                                                        <option value="1">{{ __('Low') }}</option>
                                                        <option value="2">{{ __('Medium') }}</option>
                                                        <option value="3">{{ __('High') }}</option>
                                                        <option value="4">{{ __('Urgent') }}</option>
                                                    </select>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group row">
                                                <label class="col-1 col-form-label">{{ __('Description') }}</label>
                                                <div class="col-11">
                                                    <textarea class="summernote" required>
                                                        
                                                    </textarea>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-1 col-form-label">{{ __('Service') }}</label>
                                                <div class="col-11">
                                                    <select class="selectpicker" data-style="btn-light" name="service">
                                                        <option value="A" selected="selected">{{ __('All services') }}</option>
                                                        <option value="P">{{ __('Payments') }}</option>
                                                        <?php
                                                            foreach($services as $service){
                                                                echo "<option value='". $service->id ."'>" . __($service->name) ."</option>";
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-1 col-form-label">{{ __('Attachments') }}</label>
                                                <div class="col-11">
                                                    <input type="file" data-buttonbefore="true" class="filestyle" data-btnClass="btn-dark waves-effect" name="files[]" multiple />
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                            
                                            <div class="col-9"></div>
                                                <div class="col-3">
                                                    <button type="submit" class="btn btn-success waves-effect" style="width:100%;">{{ __('Create ticket')    }}</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>
                            <!-- end row -->

                        </div> <!-- end card-box -->
                    </div><!-- end col -->
                </div>
                

@endsection


@section('hextra')

<link href="{{ URL::asset('plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="{{ URL::asset('plugins/switchery/switchery.min.css') }}" />
<link href="{{ URL::asset('plugins/summernote/summernote-bs4.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('plugins/bootstrap-fileupload/bootstrap-fileupload.css') }}" rel="stylesheet" />


@endsection

@section('fextra')
<script src="{{ URL::asset('plugins/switchery/switchery.min.js') }}"></script>
<script src="{{ URL::asset('plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js') }}"></script>
<script src="{{ URL::asset('plugins/select2/js/select2.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('plugins/bootstrap-select/js/bootstrap-select.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('plugins/bootstrap-maxlength/bootstrap-maxlength.js') }}" type="text/javascript"></script>

<script type="text/javascript" src="{{ URL::asset('plugins/autocomplete/jquery.mockjax.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('plugins/autocomplete/jquery.autocomplete.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('plugins/autocomplete/countries.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('pages/jquery.autocomplete.init.js') }}"></script>

<script type="text/javascript" src="{{ URL::asset('pages/jquery.form-advanced.init.js') }}"></script>
<script src="{{ URL::asset('plugins/bootstrap-fileupload/bootstrap-fileupload.js') }}"></script>



<script src="{{ URL::asset('plugins/summernote/summernote-bs4.min.js') }}"></script>

        <script>
            jQuery(document).ready(function(){
                $('.summernote').summernote({
                    height: 350,
                    minHeight: null,
                    maxHeight: null,
                    focus: false,
                    name: 'testing',
                    placeholder: 'Describe your issue with as many details as possible',
                    toolbar: [
                        ['style', ['bold', 'italic', 'underline', 'clear']],
                        ['font', ['strikethrough', 'superscript', 'subscript']],
                        ['fontsize', ['fontsize']],
                        ['para', ['ul', 'ol', 'paragraph']]
                    ]
                }).attr('name', 'description');
                
                $('#prio').change(function() {
                    var pr = $(this).val();
                    var cc = "btn-custom";
                    if(pr == "1"){ $(this).parent().children(".dropdown-toggle").removeClass( "btn-custom btn-primary btn-warning btn-danger" ).addClass( "btn-custom" ); }
                    if(pr == "2"){ $(this).parent().children(".dropdown-toggle").removeClass( "btn-custom btn-primary btn-warning btn-danger" ).addClass( "btn-primary" ); }
                    if(pr == "3"){ $(this).parent().children(".dropdown-toggle").removeClass( "btn-custom btn-primary btn-warning btn-danger" ).addClass( "btn-warning" ); }
                    if(pr == "4"){ $(this).parent().children(".dropdown-toggle").removeClass( "btn-custom btn-primary btn-warning btn-danger" ).addClass( "btn-danger" ); }
                });

            });
        </script>
@endsection