@extends('layouts.app')
@extends('layouts.language')
@extends('layouts.menu')
@section('subtitle')
 - Tickets
@endsection

@section('header')
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="btn-group pull-right">
                <ol class="breadcrumb hide-phone p-0 m-0">
                    <li class="breadcrumb-item"><a href="/">Evonue</a></li>
                    <li class="breadcrumb-item"><a href="/tickets">Tickets</a></li>
                    <li class="breadcrumb-item active">#ID <?php echo $tid; ?></li>
                </ol>
            </div>
            <h4 class="page-title">Tickets</h4>
        </div>
    </div>
</div>
@endsection

@section('content')
<?php 
$flag = false;
foreach($comments as $comment){ ?>
<div class="row">
    <div class="col-md-12">
        <div class="card-box ribbon-box task-detail">
        <?php $level = $ticket[0]->level;
                if($level == '1' && $flag == false){
                    echo '<div class="ribbon ribbon-custom" style="float:right;margin-left:0;margin-right:-30px;">'. __("Low") .'</div>';
                }
                if($level == '2' && $flag == false){
                    echo '<div class="ribbon ribbon-primary" style="float:right;margin-left:0;margin-right:-30px;">'. __("Medium") .'</div>';
                }
                if($level == '3' && $flag == false){
                    echo '<div class="ribbon ribbon-warning" style="float:right;margin-left:0;margin-right:-30px;">'. __("High") .'</div>';
                }
                if($level == '4' && $flag == false){
                    echo '<div class="ribbon ribbon-danger" style="float:right;margin-left:0;margin-right:-30px;">'. __("Urgent") .'</div>';
                }

            ?>

            <?php if($flag == false){ ?>
            <h3 style="font-size:28px;margin-bottom:28px;"><?php echo $ticket[0]->title; ?></h3>
            <?php } ?>
            
            <div class="media mt-0 m-b-30">
                <img class="d-flex mr-3 rounded-circle" alt="64x64" src="{{ URL::asset('avatars') }}/<?php echo $comment->avatar; ?>" style="width: 48px; height: 48px;">
                <div class="media-body" style="margin-top:4px;">
                    <h5 class="media-heading mb-0 mt-0"><?php echo $comment->name  ?></h5>
                    <span class="badge badge-info"><?php echo $comment->email; ?></span>
                </div>
            </div>
            <p style="color:#4f5255;font-size:17px;letter-spacing: 0.8px;">
                <?php echo $comment->comment; ?>
            </p>
            
            <div class="attached-files mt-4">
                <h5 class="">{{ __("Attached files") }}</h5>
                <div class="files-list">
                    <div class="file-box">
                        <a href="#"><img src="{{ URL::asset('images/file_icons/file.svg') }}" style="max-height:46px;max-width:46px;" class="img-responsive img-thumbnail" alt="attached-img"></a>
                        <p class="font-13 mb-1 text-muted"><small>File one</small></p>
                    </div>
                    <div class="file-box">
                        <a href="#"><img src="{{ URL::asset('images/attached-files/img-2.jpg') }}" style="max-height:46px;max-width:46px;" class="img-responsive img-thumbnail" alt="attached-img"></a>
                        <p class="font-13 mb-1 text-muted"><small>Attached-2</small></p>
                    </div>
                    <div class="file-box">
                        <a href="#"><img src="{{ URL::asset('images/attached-files/img-3.jpg') }}" style="max-height:46px;max-width:46px;" class="img-responsive img-thumbnail" alt="attached-img"></a>
                        <p class="font-13 mb-1 text-muted"><small>Dribbble shot</small></p>
                    </div>
                    <div class="file-box ml-3">
                        <a href="#"><span class="add-new-plus"><i class="mdi mdi-plus"></i> </span></a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<?php $flag = true; } ?>
@endsection


@section('hextra')
<link href="{{ URL::asset('plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css') }}" rel="stylesheet" />
@endsection

@section('fextra')
<script src="{{ URL::asset('plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js') }}"></script>
@endsection