<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>EVONUƎ - Login</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
	<meta content="Coderthemes" name="author" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />

	<!-- App favicon -->
	<link rel="shortcut icon" href="{{ URL::asset('favicon.png') }}">

	<!-- App css -->
	<link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ URL::asset('css/icons.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ URL::asset('css/style.css') }}" rel="stylesheet" type="text/css" />

	<script src="{{ URL::asset('js/modernizr.min.js') }}"></script>

</head>

<body>

	<!-- Begin page -->
	<div class="accountbg" style="background: url('images/bg.jpg');background-size: cover;"></div>

	<div class="wrapper-page account-page-full">

		<div class="card">
			<div class="card-block">

				<div class="account-box">

					<div class="card-box p-5">
						<h2 class="text-uppercase text-center pb-4">
							<a href="index.html" class="text-success">
								<span><img src="{{ URL::asset('images/evonue_logo.png') }}" alt="" height="26"></span>
							</a>
						</h2>

            <form class="" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}
							<div class="form-group m-b-20 row">
								<div class="col-12">
									<label for="emailaddress">Email address</label>
									<input id="email" type="email" class="form-control" name="email" placeholder="Enter your email" required autofocus>
                  @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                    @endif
								</div>
							</div>

							<div class="form-group row m-b-20">
								<div class="col-12">
									<a href="page-recoverpw.html" class="text-muted pull-right"><small>Forgot your password?</small></a>
									<label for="password">Password</label>
									<input id="password" type="password" class="form-control" name="password" placeholder="Enter your password" autocomplete="off" required>
                  @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
							</div>

							<div class="form-group row m-b-20">
								<div class="col-12">

									<div class="checkbox checkbox-custom">
										<input id="remember" type="checkbox">
										<label for="remember">
											Remember me
										</label>
									</div>

								</div>
							</div>

							<div class="form-group row text-center m-t-10">
								<div class="col-12">
									<button class="btn btn-block btn-custom waves-effect waves-light" type="submit">Sign In</button>
								</div>
							</div>

						</form>

					</div>
				</div>

			</div>
		</div>

		<div class="m-t-40 text-center">
			<p class="account-copyright">2018 © Evonue. - evonue.com</p>
		</div>

	</div>


	<!-- jQuery  -->
	<script src="{{ URL::asset('js/jquery.min.js') }}"></script>
	<script src="{{ URL::asset('js/popper.min.js') }}"></script>
	<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
	<script src="{{ URL::asset('js/waves.js') }}"></script>
	<script src="{{ URL::asset('js/jquery.slimscroll.js') }}"></script>

	<!-- App js -->
	<script src="{{ URL::asset('js/jquery.core.js') }}"></script>
	<script src="{{ URL::asset('js/jquery.app.js') }}"></script>

</body>
</html>