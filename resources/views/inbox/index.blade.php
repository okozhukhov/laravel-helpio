@extends('layouts.app')
@extends('layouts.language')
@extends('layouts.menu')
@section('subtitle')
 - Inbox
@endsection

@section('header')
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="btn-group pull-right">
                <ol class="breadcrumb hide-phone p-0 m-0">
                    <li class="breadcrumb-item"><a href="/">Evonue</a></li>
                    <li class="breadcrumb-item active">Email inbox</li>
                </ol>
            </div>
            <h4 class="page-title">Inbox</h4>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row">    
    <?php foreach($emails as $email){ ?>
    <div class="col-xs-12 col-md-6 col-lg-6 col-xl-4" style="margin:0 auto;">
        <div class="card-box tilebox-one">
            <i class="fa fa-envelope float-right text-muted"></i>
            <h6 class="text-muted text-uppercase mt-0">{{ __("Local") }}</h6>
            <h2 class="m-b-20" data-plugin="counterup"><?php echo $email->email ?></h2>
            <a href="/inbox/<?php echo strrev(base64_encode($email->email)); ?>/" type="button" class="btn btn-block btn-purple waves-effect waves-light">Open inbox</a>
        </div>
    </div>
    <?php } ?>

     <?php if (empty($email)) { ?>
        <div class="col-sm-6" style="margin:0 auto;">
            <div class="card m-b-30 card-body">
                <p class="lead m-t-0">{{ __("You have no email services") }}</p>
                <p class="card-text">{{ __("You have no available linked email services to your account please contact us via ticket to request an email service.") }}</p>
            </div>
        </div>
     <?php } ?>
</div>        
@endsection

@section('hextra')

@endsection

@section('fextra')

@endsection