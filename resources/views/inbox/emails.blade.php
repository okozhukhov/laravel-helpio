@extends('layouts.app')
@extends('layouts.language')
@extends('layouts.menu')
@section('subtitle')
 - Inbox
@endsection

@section('header')
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="btn-group pull-right">
                <ol class="breadcrumb hide-phone p-0 m-0">
                    <li class="breadcrumb-item"><a href="/">Evonue</a></li>
                    <li class="breadcrumb-item active">Email inbox</li>
                    <li class="breadcrumb-item active"><?php echo $em; ?></li>
                </ol>
            </div>
            <h4 class="page-title">Inbox</h4>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="col-lg-12">
    <div class="card-box">
        <!-- Left sidebar -->
        <div class="inbox-leftbar">

            <a href="email-compose.html" class="btn btn-danger btn-block waves-effect waves-light">Compose</a>

            <div class="mail-list mt-4">
<a href="#" class="list-group-item border-0 text-danger"><i class="mdi mdi-inbox font-18 align-middle mr-2"></i>Inbox<?php if($unread != 0){ ?><span class="badge badge-primary float-right ml-2 mt-1"><?php echo $unread; ?></span><?php } ?></a>
                <a href="#" class="list-group-item border-0"><i class="mdi mdi-send font-18 align-middle mr-2"></i>Sent Mail</a>
                <a href="#" class="list-group-item border-0"><i class="mdi mdi-delete font-18 align-middle mr-2"></i>Trash</a>
            </div>

        </div>
        <!-- End Left sidebar -->

        <div class="inbox-rightbar">

            <div class="" role="toolbar">
                <div class="btn-group">
                    <button type="button" class="btn btn-sm btn-light waves-effect"><i class="mdi mdi-archive font-18 vertical-middle"></i></button>
                    <button type="button" class="btn btn-sm btn-light waves-effect"><i class="mdi mdi-alert-octagon font-18 vertical-middle"></i></button>
                    <button type="button" class="btn btn-sm btn-light waves-effect"><i class="mdi mdi-delete-variant font-18 vertical-middle"></i></button>
                </div>
                <div class="btn-group">
                    <button type="button" class="btn btn-sm btn-light dropdown-toggle waves-effect" data-toggle="dropdown" aria-expanded="false">
                        <i class="mdi mdi-folder font-18 vertical-middle"></i>
                        <b class="caret m-l-5"></b>
                    </button>
                    <div class="dropdown-menu">
                        <span class="dropdown-header">Move to</span>
                        <a class="dropdown-item" href="javascript: void(0);">Social</a>
                        <a class="dropdown-item" href="javascript: void(0);">Promotions</a>
                        <a class="dropdown-item" href="javascript: void(0);">Updates</a>
                        <a class="dropdown-item" href="javascript: void(0);">Forums</a>
                    </div>
                </div>
                <div class="btn-group">
                    <button type="button" class="btn btn-sm btn-light dropdown-toggle waves-effect" data-toggle="dropdown" aria-expanded="false">
                        <i class="mdi mdi-label font-18 vertical-middle"></i>
                        <b class="caret m-l-5"></b>
                    </button>
                    <div class="dropdown-menu">
                        <span class="dropdown-header">Label as:</span>
                        <a class="dropdown-item" href="javascript: void(0);">Updates</a>
                        <a class="dropdown-item" href="javascript: void(0);">Social</a>
                        <a class="dropdown-item" href="javascript: void(0);">Promotions</a>
                        <a class="dropdown-item" href="javascript: void(0);">Forums</a>
                    </div>
                </div>

                <div class="btn-group">
                    <button type="button" class="btn btn-sm btn-light dropdown-toggle waves-effect" data-toggle="dropdown" aria-expanded="false">
                        <i class="mdi mdi-dots-horizontal font-18 vertical-middle"></i> More
                    </button>
                    <div class="dropdown-menu">
                        <span class="dropdown-header">More Option :</span>
                        <a class="dropdown-item" href="javascript: void(0);">Mark as Unread</a>
                        <a class="dropdown-item" href="javascript: void(0);">Add to Tasks</a>
                        <a class="dropdown-item" href="javascript: void(0);">Add Star</a>
                        <a class="dropdown-item" href="javascript: void(0);">Mute</a>
                    </div>
                </div>
            </div>

            <div class="">
                <div class="mt-4">
                    <div class="">
                        <ul class="message-list">
                        <?php foreach($data as $key=>$d){ //var_dump($d); ?>

                            <li <?php if($d->status == 0){ echo "class=\"unread\""; } ?>>
                                <a href="/email/<?php echo strrev(base64_encode($em)); ?>/<?php echo $d->messageId; ?>">
                                    <div class="col-mail col-mail-1">
                                        <div class="checkbox-wrapper-mail">
                                            <input type="checkbox" id="chk<?php echo $key; ?>" name="<?php echo $d->messageId; ?>">
                                            <label for="chk<?php echo $key; ?>" class="toggle"></label>
                                        </div>
                                        <p class="title" style="font-size:12px;"><?php echo $d->fromAddress; ?></p>
                                    </div>
                                    <div class="col-mail col-mail-2">
                                        <div class="subject"><?php echo $d->subject; ?> &nbsp;&ndash;&nbsp;
                                            <span class="teaser"><?php echo $d->summary; ?></span>
                                        </div>
                                        <?php 
                                            
                                        ?>
                                        <div class="date" style="font-size:12px;"><?php echo date('m/d/Y',($d->receivedTime / 1000)); ?></div>
                                    </div>
                                </a>
                            </li>

                        <?php } ?>
                        </ul>
                    </div>

                </div> <!-- panel body -->
            </div> <!-- panel -->

            <div class="row">
                <div class="col-7">
                    
                </div>
                <div class="col-5">
                    <div class="btn-group float-right">
                        <button type="button" class="btn btn-custom waves-light waves-effect btn-sm"><i class="fa fa-chevron-left"></i></button>
                        <button type="button" class="btn btn-custom waves-effect waves-light btn-sm"><i class="fa fa-chevron-right"></i></button>
                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>
    </div>

</div> <!-- end Col -->

@endsection

@section('hextra')

@endsection

@section('fextra')

@endsection