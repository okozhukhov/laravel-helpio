<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();


//Auth check
Route::group(['middleware' => ['auth']], function(){

    Route::get('/', 'HomeController@index')->name('home');

    Route::get('locale/{lang}', ['as'=>'lang.switch', 'uses'=>'LanguageController@change']);
    
    Route::resource('tickets', 'TicketController');
    Route::get('tickets/view/{id}', ['as'=>'view.show', 'uses'=>'TicketController@show']);

    Route::resource('inbox', 'InboxController');
    Route::get('inbox/{id}', ['as'=>'view.show', 'uses'=>'InboxController@show']);
    Route::get('email/{email}/{id}', ['as'=>'view.view', 'uses'=>'InboxController@view']);
});
