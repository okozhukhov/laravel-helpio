<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->increments('id');
			$table->string('cid');
			$table->string('name');
			$table->string('priority')->default('0');
			$table->string('url');
			$table->string('description');
			
            $table->timestamps();
        });

        Schema::create('charges', function (Blueprint $table) {
            $table->increments('id');
			$table->string('sid');
			$table->string('amount');
			$table->string('type');
			$table->string('cycle');
			$table->string('priority')->default('M');
			$table->string('note');
			
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('services');
        Schema::drop('charges');
    }
}
