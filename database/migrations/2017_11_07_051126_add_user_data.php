<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function($table) {
            $table->integer('staff')->default(0);
			$table->string('phone');
			$table->string('cellphone');
			$table->string('address');
			$table->string('area');
			$table->string('code');
			$table->string('country');
			$table->longtext('social');
			$table->longtext('notes');
			$table->string('avatar')->default('avatar.png');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function($table) {
            $table->dropColumn('level');
            $table->dropColumn('phone');
            $table->dropColumn('cellphone');
            $table->dropColumn('address');
            $table->dropColumn('area');
            $table->dropColumn('code');
            $table->dropColumn('country');
            $table->dropColumn('social');
            $table->dropColumn('notes');
            $table->dropColumn('avatar');
        });
    }
}
